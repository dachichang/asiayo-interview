SELECT property.id, property.name, count(1) as count 
FROM orders, room, property 
WHERE orders.room_id = room.id 
    AND room.property_id = property.id 
    AND FROM_UNIXTIME(orders.created_at) >= '2022-02-01' 
    AND FROM_UNIXTIME(orders.created_at) < '2022-03-01' 
GROUP BY property.id 
ORDER BY count DESC, property.id DESC;
