def exchange_convert(amount: float, rate: float):
    return "{:,.2f}".format(amount * rate)
