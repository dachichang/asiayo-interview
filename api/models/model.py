from pydantic import BaseModel, Field
from enum import Enum


class CurrencyList(str, Enum):
    TWD = 'TWD'
    JPY = 'JPY'
    USD = 'USD'


class CurrencyExchange(BaseModel):
    TWD: float
    JPY: float
    USD: float


class Currencies(BaseModel):
    TWD: CurrencyExchange
    JPY: CurrencyExchange
    USD: CurrencyExchange


class ExchangeRateResponse(BaseModel):
    currencies: Currencies


class ConvertResultResponse(BaseModel):
    result: str


class ErrorResponse(BaseModel):
    message: str


class User(BaseModel):
    username: str
    full_name: str


class Token(BaseModel):
    access_token: str
    token_type: str = 'bearer'
