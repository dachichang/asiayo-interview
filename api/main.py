import json
from datetime import datetime, timedelta
from typing import Optional
from fastapi import Depends, FastAPI, status, Query, HTTPException
from fastapi.responses import RedirectResponse, JSONResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from models import model
from lib import exchange

SECRET_KEY = "i love you"
ALGORITHM = 'HS256'
KEY_EXPIRE_MINUTES = 3

users_db = [
    {
        "username": "dachichang",
        "full_name": "Dachi Chang",
        "hashed_pwd": "$2b$12$LqMFRliXiGIdU4kTvAyLA.PuAzS5dxX1sudwbPR9iX5Ax88xKiWhO" # 12345
    }
]


with open('data/exchange_rate.json') as f:
    exchange_rate = json.load(f)

app = FastAPI()
pwd_context = CryptContext(schemes=['bcrypt'])
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def authenticate_user(username: str, password: str) -> Optional[model.User]:
    for user in users_db:
        if user['username'] == username and pwd_context.verify(password, user['hashed_pwd']):
            return model.User(username=user['username'], full_name=user['full_name'])
    return None


def get_user(token: str = Depends(oauth2_scheme)) -> Optional[model.User]:
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=ALGORITHM)
    except JWTError as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"{e}",
            headers={"WWW-Authenticate": "Bearer"}
        )

    username = payload.get("sub")
    for user in users_db:
        if username == user['username']:
            return model.User(username=user['username'], full_name=user['full_name'])
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="user not exist",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return None


def create_access_token(user: model.User) -> str:
    data = {
        "sub": user.username,
        "exp": datetime.utcnow() + timedelta(minutes=KEY_EXPIRE_MINUTES)
    }
    return jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)


# authorization
@app.post('/token')
async def generate_access_token(form_data: OAuth2PasswordRequestForm = Depends(OAuth2PasswordRequestForm)):
    user = authenticate_user(form_data.username, form_data.password)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="username or passwd wrong",
            headers={"WWW-Authenticate": "Bearer"}
        )
    access_token = create_access_token(user)
    return model.Token(access_token=access_token)


@app.get('/users/me', response_model=model.User)
async def show_me(user: model.User = Depends(get_user)):
    if user is not None:
        return user


@app.get('/')
async def root():
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content="AsiaYo interview"
    )


@app.get('/exchange_rate', responses={status.HTTP_200_OK: {"model": model.ExchangeRateResponse}})
async def get_exchange_rate():
    return model.ExchangeRateResponse(currencies=exchange_rate['currencies'])


@app.get('/convert', responses={status.HTTP_200_OK: {"model": model.ConvertResultResponse},
                                status.HTTP_400_BAD_REQUEST: {"model": model.ErrorResponse}})
async def get_exchange_converted_result(amount: int = Query(None, ge=0),
                                        from_currency: model.CurrencyList = Query(..., alias="from"),
                                        to_currency: model.CurrencyList = Query(..., alias="to"),
                                        user: model.User = Depends(get_user)):
    try:
        rate = exchange_rate['currencies'][from_currency][to_currency]
    except:
        return model.ErrorResponse(message="not exist exchange rate data")
    return model.ConvertResultResponse(result=exchange.exchange_convert(amount, rate))
