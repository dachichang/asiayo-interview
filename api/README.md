# Exchange convert API

## Prepare Environment

1. You need to have pipenv tool and python 3.9
2. run install dev environment
    - pipenv install
    - pipenv install --dev
    - pipenv shell (get virtual env)

## Execute unittest and API test

- python -m pytest -v

## Build Image and Run API

1. change to api/ directory
2. run build command
    - docker build -t asiayo-interview-exchange-api:1.0 -f docker/Dockerfile .
3. make sure **asiayo-interview-exchange-api:1.0** docker image is builded in your local docker environment
4. run api via docker compose
    - cd docker
    - docker compose up
5. try and validation api is working
    - curl '127.0.0.1' (get "AsiaYo interview")
    - curl '127.0.0.1/convert?amount=100&from=USD&to=TWD' (get result: "3,044.40")

## API Document

API document as coded, I defined the model for request input and response, through the document we can easy to realize the schema and api response

- swagger
    - http://127.0.0.1/docs
- redoc
    - http://127.0.0.1/redoc

## Result Reference

- https://imgur.com/a/UD4ynED
