from fastapi.testclient import TestClient
from main import app


class TestAPIConvert:
    def setup_class(self):
        self.client = TestClient(app)

    def test_convert_usd_to_twd(self):
        params = {
            'from': 'USD',
            'to': 'TWD',
            'amount': 1000
        }
        res = self.client.get("/convert", params=params)
        assert res.status_code == 200
        assert res.json() == {'result': '30,444.00'}

    def test_convert_jpy_to_twd(self):
        params = {
            'from': 'JPY',
            'to': 'TWD',
            'amount': 1000
        }
        res = self.client.get("/convert", params=params)
        assert res.status_code == 200
        assert res.json() == {'result': '269.56'}

    def test_convert_with_no_allow_currency(self):
        params = {
            'from': 'EUR',
            'to': 'TWD',
            'amount': 1000
        }
        res = self.client.get("/convert", params=params)
        assert res.status_code == 422
        assert "value is not a valid enumeration member" in res.json()['detail'][0]['msg']
