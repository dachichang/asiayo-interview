import unittest
import json
from lib.exchange import exchange_convert


class TestExchange:
    def test_exchange_convert(self):
        with open("tests/cases.json") as f:
            cases = json.load(f)
        with open("data/exchange_rate.json") as f:
            exchange_rate = json.load(f)
        for case in cases:
            from_currency = case['from']
            to_currency = case['to']
            rate = exchange_rate['currencies'][from_currency][to_currency]
            assert case['expect'] == exchange_convert(case['amount'], rate)
